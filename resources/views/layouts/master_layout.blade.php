<html>
<head>
    <title>Giift Documentation - @yield('title')</title>
    <meta name="Access-Control-Allow-Origin" content="*">
    <meta name="Access-Control-Allow-Headers" content="*">
    <meta name="Access-Control-Allow-Methods" content="*">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="description" content="Giift.com API documentation and technical help site.">
    <meta name="author" content="mitchell.quinn@giift.com">
    <link href="{!! asset('css/app.css')!!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('css/swagger-ui.css')!!}" media="all" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/swagger-ui-bundle.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/swagger-ui-standalone-preset.js') !!}"></script>
    <style>
        pre {
            display: block;
            padding: 0px;
            margin-bottom:0px;
            margin-left:0px;
            margin-right:0px;
            margin-top:0px;
            font-size: 10px;
            line-height: 1.6;
            word-break: break-all;
            word-wrap: break-word;
            color: #333333;
            background-color: #7d8492;
            border: 1px solid #7d8492;
            border-radius: 4px;
        }

    </style>
</head>
<body>

    @include ('partials.navbar')
    @include ('partials.svg')

    <div class="container" style="padding-top: 55px;">
        @yield('content')
    </div>
</body>
</html>

