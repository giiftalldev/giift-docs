<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Giift API Documentation</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="https://bitbucket.org/giiftalldev/giift-swagger/raw/master/Giift-Swagger-Postman.yaml" download>Download Postman Extension</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">API List <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @foreach (array_keys(config('pages')) as $title)
                            @php
                                $path = ucwords($title)
                            @endphp
                            <li><a href="/{{ $path }}">{{ $path }}</a></li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
