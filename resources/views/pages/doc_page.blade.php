@extends('layouts.master_layout')

@section('title', $title)

@section('content')
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
            // Build a system
            window.ui = SwaggerUIBundle({
                url: "{{ $swaggerUri }}",
                dom_id: '#swagger-ui',
                presets: [
                    SwaggerUIBundle.presets.apis,
                    SwaggerUIStandalonePreset
                ],
                plugins: [
                    SwaggerUIBundle.plugins.DownloadUrl
                ],
                layout: "StandaloneLayout"
            });
        }
    </script>
@stop
