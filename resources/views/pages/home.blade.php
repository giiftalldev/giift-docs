@extends('layouts.master_layout')

@section('title', 'Home')

@section('content')
    <div class="text-center">
        <h1>Welcome to the Giift API documentation server!</h1>
        <h2>Feel free to browse the individual APIs in the navbar.</h2>
        <h5>More info to come here in the future!</h5>
    </div>
@stop