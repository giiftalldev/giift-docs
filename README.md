# Giift Docs Application #

This is the docs applcation for: doc.giift.com.

### What is this repository for? ###

* This repository for providing documentation for the giift.com application.

### How do I get set up? ###

* Standard Laravel project
* composer install
* npm run dev
* php artisan serve - will create a local server : http://127.0.0.1:8000/

### Contribution guidelines ###

* Feel free to clone and create a pull request. I will try to get your changes merged in as soon as possible.

### Who do I talk to? ###

* mitchell.quinn@giift.com