<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function show(string $page)
    {
        $swaggerUri = $this->getSwaggerUri($page);

        if ($swaggerUri) {
            $title = ucwords($page);

            return view('pages.doc_page', compact('title', 'swaggerUri'));
        }

        abort(404, 'Sorry, the page you are looking for could not be found.');
    }

    private function getSwaggerUri($page)
    {
        $page = strtolower($page);
        $pagesList = config('pages');

        return $pagesList[$page] ?? null;
    }
}
