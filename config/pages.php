<?php

return [
  'authorization' => 'https://bitbucket.org/giiftalldev/giift-swagger/raw/master/Giift-Authorization.yaml',
  'card' => 'https://bitbucket.org/giiftalldev/giift-swagger/raw/master/Giift-Card.yaml',
  'cart' => 'https://bitbucket.org/giiftalldev/giift-swagger/raw/master/Giift-Cart.yaml',
  'common' => 'https://bitbucket.org/giiftalldev/giift-swagger/raw/master/Giift-Common.yaml',
  'dmo' => 'https://bitbucket.org/giiftalldev/giift-swagger/raw/master/Giift-Dmo.yaml',
  'exchange' => 'https://bitbucket.org/giiftalldev/giift-swagger/raw/master/Giift-Exchange.yaml',
  'report' => 'https://bitbucket.org/giiftalldev/giift-swagger/raw/master/Giift-Report.yaml',
  'user' => 'https://bitbucket.org/giiftalldev/giift-swagger/raw/master/Giift-User.yaml',
];
